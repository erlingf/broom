package no.fjelstad.erling.broom.models

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

@Dao
interface NoteDao {


    @get:Query("Select * from Note")
    val allNotes: LiveData<List<Note>>

    @Insert
    fun insert(vararg note: Note)

    @Update
    fun update(vararg note: Note)

    @Delete
    fun delete(vararg note: Note)

    @Query("Select * from Note WHERE :noteId=id")
    fun getNoteLiveData(noteId: Long): LiveData<Note>

}
