package no.fjelstad.erling.broom.editor

import dagger.Component
import no.fjelstad.erling.broom.dagger.FragmentScope

@FragmentScope
@Component(modules = [(NoteEditorModule::class)])
interface NoteEditorComponent {
    fun inject(noteEditorFragment: NoteEditorFragment)
}
