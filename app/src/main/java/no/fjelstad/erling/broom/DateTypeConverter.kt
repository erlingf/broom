package no.fjelstad.erling.broom

import android.arch.persistence.room.TypeConverter

import java.text.ParseException
import java.util.Date

import no.fjelstad.erling.broom.App.simpleDateFormat

object DateTypeConverter {

    @TypeConverter
    @Throws(ParseException::class)
    fun toDate(value: String?): Date? {
        return if (value == null) null else simpleDateFormat.parse(value)
    }

    @TypeConverter
    fun toString(value: Date?): String? {
        return if (value == null) null else simpleDateFormat.format(value)
    }

}
