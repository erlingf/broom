package no.fjelstad.erling.broom.editor

import no.fjelstad.erling.broom.common.View
import no.fjelstad.erling.broom.models.Note

interface NoteEditorView : View {
    fun renderNote(note: Note?)

    fun onNoteSaved()
}
