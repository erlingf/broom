package no.fjelstad.erling.broom.notelist

import dagger.Component
import no.fjelstad.erling.broom.dagger.FragmentScope

@Component(modules = [(NoteListModule::class)])
@FragmentScope
interface NoteListComponent {
    fun inject(noteListFragment: NoteListFragment)
}
