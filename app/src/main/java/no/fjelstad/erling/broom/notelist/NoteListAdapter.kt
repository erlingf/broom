package no.fjelstad.erling.broom.notelist

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotterknife.bindView
import no.fjelstad.erling.broom.R
import no.fjelstad.erling.broom.models.Note
import java.util.*

class NoteListAdapter
internal constructor(private val context: Context,
                     private val onNoteClickedListener: OnNoteClickedListener,
                     private val onNoteDeleteClickListener: OnNoteDeleteClickListener)
    : RecyclerView.Adapter<NoteListAdapter.ViewHolder>() {
    private val notes: MutableList<Note>

    init {
        this.notes = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.recycler_view_note, parent, false)
        return ViewHolder(view, onNoteClickedListener, onNoteDeleteClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.update(notes[position])
    }


    override fun getItemCount(): Int {
        return notes.size
    }

    fun swapData(list: List<Note>) {
        notes.clear()
        notes.addAll(list)

        notifyDataSetChanged()
    }

    interface OnNoteClickedListener {
        fun onNoteClicked(note: Note)
    }

    interface OnNoteDeleteClickListener {
        fun onNoteDeleted(note: Note)
    }

    class ViewHolder(itemView: View, outerNoteClickListener: OnNoteClickedListener,
                     onNoteDeleteClickListener: OnNoteDeleteClickListener) :
            RecyclerView.ViewHolder(itemView) {

        // views
        private val viewContainer: View by bindView(R.id.recycler_view_note_container)

        private val titleTextView: AppCompatTextView by bindView(R.id.title_text_view)

        private val contentTextView: AppCompatTextView by bindView(R.id.content_text_view)

        private val removeButton: AppCompatImageView by bindView(R.id.image_button_remove_note)

        private val internalClickListener = InternalClickListener(outerNoteClickListener)
        private val internalImageButtonClickListener
                = InternalImageButtonClickListener(onNoteDeleteClickListener)

        fun update(note: Note) {
            // update onClickListeners with note
            internalClickListener.update(note)
            internalImageButtonClickListener.update(note)

            titleTextView.text = note.title
            contentTextView.text = note.content

            viewContainer.setOnClickListener(internalClickListener)
            removeButton.setOnClickListener(internalImageButtonClickListener)
        }
    }

    private class InternalClickListener
    internal constructor(private val onNoteClickedListener: OnNoteClickedListener) :
            View.OnClickListener {

        private lateinit var note: Note

        override fun onClick(v: View) {
            onNoteClickedListener.onNoteClicked(note)
        }

        internal fun update(note: Note) {
            this.note = note
        }
    }

    private class InternalImageButtonClickListener
    internal constructor(private val onNoteDeleteClickListener: OnNoteDeleteClickListener)
        : View.OnClickListener {

        private lateinit var note: Note

        override fun onClick(v: View) {
            onNoteDeleteClickListener.onNoteDeleted(note)
        }

        internal fun update(note: Note) {
            this.note = note
        }
    }
}
