package no.fjelstad.erling.broom.editor

import no.fjelstad.erling.broom.common.Presenter
import no.fjelstad.erling.broom.models.Note

interface NoteEditorPresenter : Presenter {
    fun saveNote(note: Note, title: String, content: String)
}
