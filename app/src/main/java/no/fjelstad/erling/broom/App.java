package no.fjelstad.erling.broom;

import android.app.Application;
import android.content.Context;

import java.text.SimpleDateFormat;

import no.fjelstad.erling.broom.dagger.AppComponent;
import no.fjelstad.erling.broom.dagger.AppModule;
import no.fjelstad.erling.broom.dagger.DaggerAppComponent;


public class App extends Application {
    private AppComponent appComponent;
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "my_db";
    static SimpleDateFormat simpleDateFormat =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", LocaleUtils.Companion.getLocale());

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();


    }


    public AppComponent appComponent() {
        return appComponent;
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }


}
