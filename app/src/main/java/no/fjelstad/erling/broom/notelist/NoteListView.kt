package no.fjelstad.erling.broom.notelist

import no.fjelstad.erling.broom.common.View
import no.fjelstad.erling.broom.models.Note

interface NoteListView : View {
    fun renderNotes(notes: List<Note>)

    fun onNewNoteCreated(note: Note)

    fun onNoteDeleted(noteTitle: String)
}
