package no.fjelstad.erling.broom.dagger

import android.content.Context

import dagger.Component

@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun context(): Context

    fun dbModule(): DbModule
}
