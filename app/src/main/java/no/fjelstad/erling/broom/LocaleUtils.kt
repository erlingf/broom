package no.fjelstad.erling.broom

import java.util.*

class LocaleUtils {
    companion object {
        val locale: Locale
            get() = Locale.US
    }
}
