package no.fjelstad.erling.broom.editor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import no.fjelstad.erling.broom.R;
import no.fjelstad.erling.broom.Utils;
import no.fjelstad.erling.broom.models.Note;

public class NoteEditorActivity extends AppCompatActivity {
    public static final String ARG_NOTE_TRANSFER_MODEL = "arg:NoteTransferModel";
    NoteEditorFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            Note transferModel = getIntent()
                    .getParcelableExtra(ARG_NOTE_TRANSFER_MODEL);
            fragment = NoteEditorFragment.Companion.newInstance(transferModel);
            Utils.Companion.attachFragment(fragmentManager, fragment, ARG_NOTE_TRANSFER_MODEL);

        } else {
            fragment = (NoteEditorFragment) fragmentManager.findFragmentByTag(ARG_NOTE_TRANSFER_MODEL);
            Utils.Companion.attachFragment(fragmentManager, fragment, ARG_NOTE_TRANSFER_MODEL);
        }


    }
}
