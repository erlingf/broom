package no.fjelstad.erling.broom.common

interface Presenter {

    fun attach(view: View)

    fun detach()
}
