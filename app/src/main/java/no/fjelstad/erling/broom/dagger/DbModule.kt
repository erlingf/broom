package no.fjelstad.erling.broom.dagger

import android.arch.persistence.room.Room
import android.content.Context

import dagger.Module
import dagger.Provides
import no.fjelstad.erling.broom.RoomDb
import no.fjelstad.erling.broom.models.NoteDao

@Module
@ApplicationScope
class DbModule(private val context: Context, private val dbName: String) {

    @Provides
    @ApplicationScope
    fun providesRoomDb(): RoomDb {
        return Room.databaseBuilder(context, RoomDb::class.java, dbName).build()
    }

    @Provides
    @ApplicationScope
    fun providesNoteDao(roomDb: RoomDb): NoteDao {
        return roomDb.noteDao()
    }

}
