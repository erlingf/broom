package no.fjelstad.erling.broom.notelist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import no.fjelstad.erling.broom.R;
import no.fjelstad.erling.broom.Utils;
import no.fjelstad.erling.broom.editor.NoteEditorActivity;
import no.fjelstad.erling.broom.models.Note;


public class NoteListActivity extends AppCompatActivity implements NoteListFragment.OnNoteChosen {
    public static final String ARG_NOTE_LIST_FRAGMENT = "arg:noteListFragment";
    public static final String ARG_NOTE = "arg:NoteTransferModel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();

        NoteListFragment fragment;
        if (savedInstanceState == null) {
            fragment = new NoteListFragment();
            Utils.Companion.attachFragment(fragmentManager, fragment, ARG_NOTE_LIST_FRAGMENT);

        } else {
            fragment = (NoteListFragment) fragmentManager.findFragmentByTag(ARG_NOTE_LIST_FRAGMENT);
            Utils.Companion.attachFragment(fragmentManager, fragment, ARG_NOTE_LIST_FRAGMENT);
        }
    }


    @Override
    public void navigateToEditor(@NonNull Note note) {
        Intent intent = new Intent(this, NoteEditorActivity.class);
        intent.putExtra(ARG_NOTE, note);
        startActivity(intent);
    }
}
