package no.fjelstad.erling.broom.notelist

import android.arch.lifecycle.LifecycleOwner

import no.fjelstad.erling.broom.common.Presenter
import no.fjelstad.erling.broom.models.Note

/**
 * Created by erling on 23.10.2017.
 */

interface NoteListPresenter : Presenter {
    fun getAllNotes(owner: LifecycleOwner)

    fun createNewNote()

    fun deleteNote(note: Note)
}
