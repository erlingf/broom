package no.fjelstad.erling.broom.editor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import no.fjelstad.erling.broom.common.View;
import no.fjelstad.erling.broom.models.Note;
import no.fjelstad.erling.broom.models.NoteDao;

public class NoteEditorPresenterImpl implements NoteEditorPresenter {

    @NonNull
    private CompositeDisposable disposable;

    @NonNull
    private final NoteDao noteDao;

    @Nullable
    private NoteEditorView noteEditorView;

    @Inject
    NoteEditorPresenterImpl(@NonNull NoteDao noteDao) {
        this.noteDao = noteDao;
        disposable = new CompositeDisposable();
    }

    @Override
    public void attach(@Nullable View view) {
        if (view instanceof NoteEditorView) {
            noteEditorView = (NoteEditorView) view;
        }
    }

    @Override
    public void detach() {
        disposable.clear();
        noteEditorView = null;
    }

    @Override
    public void saveNote(@NonNull final Note noteToSave,
                         @NonNull final String title,
                         @NonNull final String content) {
        Observable.just(noteToSave)
                .subscribeOn(Schedulers.io())
                .map(note -> {
                    Note noteToUpdate = new Note(title, content, note.getLastSavedTimestamp());
                    noteDao.update(noteToUpdate);
                    return noteToUpdate;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(note -> {
                    if (noteEditorView != null) {
                        noteEditorView.onNoteSaved();
                    }
                });
    }
}
