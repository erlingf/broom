package no.fjelstad.erling.broom.editor

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatEditText
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import kotterknife.bindView
import no.fjelstad.erling.broom.App
import no.fjelstad.erling.broom.R
import no.fjelstad.erling.broom.models.Note
import javax.inject.Inject

class NoteEditorFragment : Fragment(), NoteEditorView {
    private var note: Note? = null

    // views
    private val coordinatorLayout: CoordinatorLayout by bindView(R.id.coordinatorlayout_editor)

    private val editTextTitle: AppCompatEditText by bindView(R.id.edittext_title_editor)

    private val editTextEditor: AppCompatEditText by bindView(R.id.edittext_content_editor)

    private val fabSaveNote: FloatingActionButton by bindView(R.id.fab_save_note)

    @Inject
    lateinit var noteEditorPresenter: NoteEditorPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        if (arguments != null && arguments.getParcelable<Parcelable>(ARG_NOTE) != null) {
            note = arguments.getParcelable(ARG_NOTE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_note_editor, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //        ButterKnife.bind(this, view);

        val actionBar = (activity as NoteEditorActivity).supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)

        editTextTitle.setText(note!!.title)
        editTextEditor.setText(note!!.content)

        fabSaveNote.setOnClickListener {
            noteEditorPresenter.saveNote(note!!,
                    editTextTitle.text.toString(),
                    editTextEditor.text.toString())
        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        val dbModule = App.get(context).appComponent().dbModule()

        DaggerNoteEditorComponent.builder()
                .noteEditorModule(NoteEditorModule())
                .dbModule(dbModule)
                .build()
                .inject(this)

    }

    override fun onResume() {
        super.onResume()
        noteEditorPresenter.attach(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                activity.finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        super.onPause()
        noteEditorPresenter.detach()
    }

    override fun renderNote(note: Note?) {
        if (note != null) {
            editTextTitle.setText(note.title)
            editTextEditor.setText(note.content)
        }
    }

    override fun onNoteSaved() {
        Snackbar.make(coordinatorLayout, "Note is saved", Snackbar.LENGTH_LONG).show()
    }


    companion object {
        val ARG_NOTE = "arg:Note"

        fun newInstance(note: Note?): NoteEditorFragment {

            val bundle = Bundle(1)
            if (note != null) {
                bundle.putParcelable(ARG_NOTE, note)
            }

            val fragment = NoteEditorFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
