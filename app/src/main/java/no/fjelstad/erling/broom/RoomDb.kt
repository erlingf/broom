package no.fjelstad.erling.broom

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

import no.fjelstad.erling.broom.models.Note
import no.fjelstad.erling.broom.models.NoteDao

@Database(entities = [(Note::class)], version = App.DB_VERSION, exportSchema = false)
@TypeConverters(DateTypeConverter::class)
abstract class RoomDb : RoomDatabase() {
    abstract fun noteDao(): NoteDao
}
