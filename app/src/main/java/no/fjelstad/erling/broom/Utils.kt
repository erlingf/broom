package no.fjelstad.erling.broom

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

class Utils {

    companion object {
        fun attachFragment(fragmentManager: FragmentManager,
                           fragment: Fragment,
                           tag: String) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, tag).commit()
        }
    }

}
