package no.fjelstad.erling.broom.notelist

import android.content.Context
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotterknife.bindView
import no.fjelstad.erling.broom.App
import no.fjelstad.erling.broom.R
import no.fjelstad.erling.broom.models.Note
import javax.inject.Inject

/**
 * Created by erling on 23.10.2017.
 */

class NoteListFragment :
        Fragment(), NoteListView,
        NoteListAdapter.OnNoteClickedListener, NoteListAdapter.OnNoteDeleteClickListener {

    // views
    private val coordinatorLayout: CoordinatorLayout by bindView(R.id.coordinator_layout_notes)

    private val recyclerView: RecyclerView by bindView(R.id.recycler_view_notes)

    private val floatingActionButton: FloatingActionButton by bindView(R.id.fab_new_note)

    // RecyclerViewAdapter
    private var noteListAdapter: NoteListAdapter? = null


    // inject the presenter
    @Inject
    internal lateinit var noteListPresenter: NoteListPresenter

    private var callback: OnNoteChosen? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)

        DaggerNoteListComponent.builder()
                .noteListModule(NoteListModule())
                .dbModule(App.get(context).appComponent().dbModule())
                .build()
                .inject(this)
        val activity = activity
        try {
            callback = activity as OnNoteChosen
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " Must implement OnNoteChosen")
        }

    }

    override fun onResume() {
        super.onResume()
        noteListPresenter.attach(this)
        noteListPresenter.getAllNotes(this)
    }

    override fun onPause() {
        super.onPause()
        noteListPresenter.detach()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_note_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecyclerViewAdapter(view)

        floatingActionButton.setOnClickListener { noteListPresenter.createNewNote() }
    }


    private fun setUpRecyclerViewAdapter(view: View) {
        noteListAdapter = NoteListAdapter(
                recyclerView.context,
                this, this)

        recyclerView.adapter = noteListAdapter
        recyclerView.layoutManager = LinearLayoutManager(view.context)
    }

    override fun onNoteClicked(note: Note) {
        callback!!.navigateToEditor(note)
    }

    override fun renderNotes(notes: List<Note>) {
        noteListAdapter!!.swapData(notes)
    }

    override fun onNewNoteCreated(note: Note) {
        callback!!.navigateToEditor(note)
    }

    override fun onNoteDeleted(noteTitle: String) {
        Snackbar.make(coordinatorLayout,
                noteTitle + " has been deleted",
                Snackbar.LENGTH_LONG).show()
    }


    override fun onNoteDeleted(note: Note) {
        noteListPresenter.deleteNote(note)
    }

    internal interface OnNoteChosen {
        fun navigateToEditor(note: Note)
    }
}
