package no.fjelstad.erling.broom.editor

import dagger.Module
import dagger.Provides
import no.fjelstad.erling.broom.dagger.DbModule
import no.fjelstad.erling.broom.dagger.FragmentScope
import no.fjelstad.erling.broom.models.NoteDao

/**
 * Created by erling on 24.10.2017.
 */
@FragmentScope
@Module(includes = [(DbModule::class)])
class NoteEditorModule {

    @Provides
    @FragmentScope
    fun providesNoteEditorPresenter(noteDao: NoteDao): NoteEditorPresenter {
        return NoteEditorPresenterImpl(noteDao)
    }
}
