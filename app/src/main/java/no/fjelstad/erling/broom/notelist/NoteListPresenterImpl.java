package no.fjelstad.erling.broom.notelist;

import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Calendar;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import no.fjelstad.erling.broom.common.View;
import no.fjelstad.erling.broom.models.Note;
import no.fjelstad.erling.broom.models.NoteDao;


public class NoteListPresenterImpl implements NoteListPresenter {
    @NonNull
    private CompositeDisposable disposable;

    @Nullable
    private NoteListView noteListView;

    @NonNull
    private NoteDao noteDao;

    @Inject
    NoteListPresenterImpl(@NonNull NoteDao noteDao) {
        disposable = new CompositeDisposable();
        this.noteDao = noteDao;
    }

    @Override
    public void attach(@NonNull View view) {
        if (view instanceof NoteListView) {
            noteListView = (NoteListView) view;
        }
    }

    @Override
    public void detach() {
        noteListView = null;
        disposable.clear();
    }

    @Override
    public void getAllNotes(@NonNull LifecycleOwner owner) {
        noteDao.getAllNotes().observe(owner, notes -> {
            if (notes != null && noteListView != null) {
                noteListView.renderNotes(notes);
            }
        });
    }

    @Override
    public void createNewNote() {
        Note newNote = new Note("", "", Calendar.getInstance().getTime().toString());
        Observable.just(newNote)
                .subscribeOn(Schedulers.io())
                .map(note -> {
                    noteDao.insert(note);
                    return note;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(note -> {
                    if (noteListView != null) {
                        noteListView.onNewNoteCreated(note);
                    }
                });
    }

    @Override
    public void deleteNote(@NonNull Note note) {
        Observable.just(note)
                .subscribeOn(Schedulers.io())
                .map(noteToDelete -> {
                    noteDao.delete(noteToDelete);
                    return noteToDelete.getTitle();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(deletedNoteTitle -> {
                            if (noteListView != null) {
                                noteListView.onNoteDeleted(deletedNoteTitle);
                            }
                        }
                );
    }
}
