package no.fjelstad.erling.broom.models

import android.annotation.SuppressLint
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
@Entity(tableName = "Note")
data class Note(
        var title: String,
        var content: String,
        var lastSavedTimestamp: String) : Parcelable {


    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

}