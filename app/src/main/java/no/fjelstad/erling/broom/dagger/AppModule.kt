package no.fjelstad.erling.broom.dagger

import android.app.Application
import android.content.Context

import dagger.Module
import dagger.Provides
import no.fjelstad.erling.broom.App

@Module
class AppModule(application: Application) {
    private val context: Context

    init {
        this.context = application
    }

    @ApplicationScope
    @Provides
    fun providesAppContext(): Context {
        return context
    }

    @ApplicationScope
    @Provides
    fun providesDbModule(): DbModule {
        return DbModule(context, App.DB_NAME)
    }


}
