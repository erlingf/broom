package no.fjelstad.erling.broom.dagger

@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope
