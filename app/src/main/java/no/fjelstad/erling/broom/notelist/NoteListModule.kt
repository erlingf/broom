package no.fjelstad.erling.broom.notelist

import dagger.Module
import dagger.Provides
import no.fjelstad.erling.broom.dagger.DbModule
import no.fjelstad.erling.broom.dagger.FragmentScope
import no.fjelstad.erling.broom.models.NoteDao

@FragmentScope
@Module(includes = [(DbModule::class)])
class NoteListModule {

    @FragmentScope
    @Provides
    fun providesNoteListPresenter(noteDao: NoteDao): NoteListPresenter {
        return NoteListPresenterImpl(noteDao)
    }
}
